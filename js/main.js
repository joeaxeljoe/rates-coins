function beautyDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth()+1; //January is 0!
    var yyyy = date.getFullYear();
// d/m/Y H:i:s
    var HH = date.getHours()
    var II = date.getMinutes()
    var SS = date.getSeconds()


    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    if(HH<10) {
        HH = '0'+HH
    }

    if(II<10) {
        II = '0'+II
    }

    if(mm<10) {
        SS = '0'+SS
    }


   return mm + '/' + dd + '/' + yyyy + " " + HH + ":" + mm + ":" + SS;

}
