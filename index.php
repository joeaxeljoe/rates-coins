<?php
require 'vendor/autoload.php';
require 'models/ENV.php';
require 'models/Rate.php';
require 'models/Shoutbox.php';


ENV::init();

$f3 = \Base::instance();

$DB_HOST = ENV::DB_HOST();
$DB_PORT = ENV::DB_PORT();
$DB_NAME = ENV::DB_NAME();

$db = new DB\SQL (
    "mysql:host={$DB_HOST};port={$DB_PORT};dbname={$DB_NAME}",
    ENV::DB_USER(),
    ENV::DB_PASS()
);

require 'routs.php';

if(!$f3->exists('COOKIE.shoutbox_color')) {
    $f3->set('COOKIE.shoutbox_color', Shoutbox::random_color(), 60 * 24 * 365 * 5); // 5 years
}


$f3->run();