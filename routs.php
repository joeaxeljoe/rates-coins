<?php

$f3->set('BASE_URL', ENV::BASE_URL());
$f3->set('SOCKET_URL', ENV::SOCKET_URL());
$f3->set('SOCKET_PORT', ENV::SOCKET_PORT());

$f3->route('GET /history',
    function() use ($f3, $db) {

        $f3->set('coinsHistorical', Rate::getCoinsHistorical($db));

        echo \Template::instance()->render('views/historical.html');
    }
);

$f3->route('GET /',
    function() use ($f3, $db) {


        $coinsRawData = Rate::getCurrently($db);
        $coinsData = [];

        foreach($coinsRawData as $coin) {
            if(!isset($coinsData[$coin['currency']])) {
                $coinsData[$coin['currency']] = [];
            }

            $coinsData[$coin['currency']][$coin['source_api']] = $coin['price_USD'];
            $coinsData[$coin['currency']]['currency'] = $coin['currency'];

            if(isset($coinsData[$coin['currency']]['coin-market-cap']) && isset($coinsData[$coin['currency']]['binance'])) {
                $coinsData[$coin['currency']]['AVG'] = ($coinsData[$coin['currency']]['coin-market-cap'] +  $coinsData[$coin['currency']]['binance']) / 2;
            }
        }



        $f3->set('coinsData', $coinsData);
        $f3->set('shoutboxMessages', Shoutbox::getLasts($db));

        echo \Template::instance()->render('views/home.html');
    }
);
