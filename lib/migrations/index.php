<?php
require 'vendor/autoload.php';

if(!class_exists('ENV')) {
    require 'models/ENV.php';
    ENV::init();
}

if(!isset($db)) {
    $DB_HOST = ENV::DB_HOST();
    $DB_PORT = ENV::DB_PORT();
    $DB_NAME = ENV::DB_NAME();

    $db = new DB\SQL (
        "mysql:host={$DB_HOST};port={$DB_PORT};dbname={$DB_NAME}",
        ENV::DB_USER(),
        ENV::DB_PASS()
    );
}


if(defined('RESET_DB') && RESET_DB) {
    # rate table
    $db->exec("DROP TABLE IF EXISTS `rate`;");
    $db->exec("DROP TABLE IF EXISTS `group_rate`;");
    $db->exec("DROP TABLE IF EXISTS `shoutbox`;");

    echo "DB reset successfully\n";
}


# group_rate table
$db->exec(
    "
    CREATE TABLE IF NOT EXISTS `group_rate` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `status` int(11) NOT NULL DEFAULT '1',
      `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
    "
);

# rate table
$db->exec(
    "
    CREATE TABLE IF NOT EXISTS `rate` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `currency` varchar(64) NOT NULL,
      `price_USD` double NOT NULL,
      `source_api` varchar(63) NOT NULL,
      `group_rate_id` int(11) NOT NULL,
      `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `group_rate_id` (`group_rate_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=utf8;
    "
);


# shoutbox table
$db->exec(
    "
    CREATE TABLE IF NOT EXISTS `shoutbox` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `msg` text NOT NULL,
      `user` varchar(64) DEFAULT NULL,
      `color` varchar(9) NOT NULL,
      `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    ) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
    "
);

echo "migrations run successfully\n";


if(defined('SEED') && SEED) {
    $db->exec("
            INSERT INTO `shoutbox` (`id`, `msg`, `user`, `color`, `create_at`) VALUES
                (8, 'The Beatiquin went down a lot!', 'Joe', '#00a8f5', '2018-12-02 16:20:27'),
                (9, 'Do not worry about a friend, it\'s natural. Now is the most appropriate time to purchase a little :-)', 'Alecsis', '#4e2ee7', '2018-12-02 16:32:41'),
                (10, 'Yes, it looks like the ETH will be the right choice now', 'Rachel', '#76833b', '2018-12-02 16:34:11');
        ");

    $db->exec("
            INSERT INTO `group_rate` (`id`, `status`, `create_at`) VALUES
                (74, 1, '2018-12-02 16:19:05'),
                (75, 1, '2018-12-02 16:20:06'),
                (76, 1, '2018-12-02 16:21:08'),
                (77, 1, '2018-12-02 16:22:09'),
                (78, 1, '2018-12-02 16:23:10'),
                (79, 1, '2018-12-02 16:24:11'),
                (80, 1, '2018-12-02 16:25:12'),
                (81, 1, '2018-12-02 16:26:13'),
                (82, 1, '2018-12-02 16:27:14'),
                (83, 1, '2018-12-02 16:28:15'),
                (84, 1, '2018-12-02 16:29:16'),
                (85, 1, '2018-12-02 16:30:18'),
                (86, 1, '2018-12-02 16:31:19'),
                (87, 1, '2018-12-02 16:32:20'),
                (88, 1, '2018-12-02 16:33:21'),
                (89, 1, '2018-12-02 16:34:22'),
                (90, 1, '2018-12-02 16:35:23');
        ");

    $db->exec("
            INSERT INTO `rate` (`id`, `currency`, `price_USD`, `source_api`, `group_rate_id`, `create_at`) VALUES
                (350, 'BTC', 4167.57, 'binance', 74, '2018-12-02 16:19:05'),
                (351, 'ETH', 117.19, 'binance', 74, '2018-12-02 16:19:05'),
                (352, 'LTC', 33.76565214, 'binance', 74, '2018-12-02 16:19:05'),
                (353, 'BTC', 4141.57728964, 'coin-market-cap', 74, '2018-12-02 16:19:05'),
                (354, 'ETH', 116.739794106, 'coin-market-cap', 74, '2018-12-02 16:19:05'),
                (355, 'LTC', 33.6980760004, 'coin-market-cap', 74, '2018-12-02 16:19:05'),
                (356, 'BTC', 4169.95, 'binance', 75, '2018-12-02 16:20:06'),
                (357, 'ETH', 117.34, 'binance', 75, '2018-12-02 16:20:06'),
                (358, 'LTC', 33.76408515, 'binance', 75, '2018-12-02 16:20:06'),
                (359, 'BTC', 4151.33093705, 'coin-market-cap', 75, '2018-12-02 16:20:06'),
                (360, 'ETH', 116.739794106, 'coin-market-cap', 75, '2018-12-02 16:20:06'),
                (361, 'LTC', 33.6980760004, 'coin-market-cap', 75, '2018-12-02 16:20:06'),
                (362, 'BTC', 4169.98, 'binance', 76, '2018-12-02 16:21:08'),
                (363, 'ETH', 117.15, 'binance', 76, '2018-12-02 16:21:08'),
                (364, 'LTC', 33.7351382, 'binance', 76, '2018-12-02 16:21:08'),
                (365, 'BTC', 4151.33093705, 'coin-market-cap', 76, '2018-12-02 16:21:08'),
                (366, 'ETH', 116.836040014, 'coin-market-cap', 76, '2018-12-02 16:21:08'),
                (367, 'LTC', 33.7229053149, 'coin-market-cap', 76, '2018-12-02 16:21:08'),
                (368, 'BTC', 4164.11, 'binance', 77, '2018-12-02 16:22:09'),
                (369, 'ETH', 117.02, 'binance', 77, '2018-12-02 16:22:09'),
                (370, 'LTC', 33.72096278, 'binance', 77, '2018-12-02 16:22:09'),
                (371, 'BTC', 4157.74061324, 'coin-market-cap', 77, '2018-12-02 16:22:09'),
                (372, 'ETH', 116.836040014, 'coin-market-cap', 77, '2018-12-02 16:22:09'),
                (373, 'LTC', 33.7229053149, 'coin-market-cap', 77, '2018-12-02 16:22:09'),
                (374, 'BTC', 4164.57, 'binance', 78, '2018-12-02 16:23:10'),
                (375, 'ETH', 117.08, 'binance', 78, '2018-12-02 16:23:10'),
                (376, 'LTC', 33.70386501, 'binance', 78, '2018-12-02 16:23:10'),
                (377, 'BTC', 4157.74061324, 'coin-market-cap', 78, '2018-12-02 16:23:10'),
                (378, 'ETH', 116.836040014, 'coin-market-cap', 78, '2018-12-02 16:23:10'),
                (379, 'LTC', 33.7229053149, 'coin-market-cap', 78, '2018-12-02 16:23:10'),
                (380, 'BTC', 4164.8, 'binance', 79, '2018-12-02 16:24:11'),
                (381, 'ETH', 117.06, 'binance', 79, '2018-12-02 16:24:11'),
                (382, 'LTC', 33.7182208, 'binance', 79, '2018-12-02 16:24:11'),
                (383, 'BTC', 4157.74061324, 'coin-market-cap', 79, '2018-12-02 16:24:11'),
                (384, 'ETH', 116.900443126, 'coin-market-cap', 79, '2018-12-02 16:24:11'),
                (385, 'LTC', 33.6024432942, 'coin-market-cap', 79, '2018-12-02 16:24:11'),
                (386, 'BTC', 4167.66, 'binance', 80, '2018-12-02 16:25:12'),
                (387, 'ETH', 117.09, 'binance', 80, '2018-12-02 16:25:12'),
                (388, 'LTC', 33.74137536, 'binance', 80, '2018-12-02 16:25:12'),
                (389, 'BTC', 4151.50091003, 'coin-market-cap', 80, '2018-12-02 16:25:12'),
                (390, 'ETH', 116.900443126, 'coin-market-cap', 80, '2018-12-02 16:25:12'),
                (391, 'LTC', 33.6024432942, 'coin-market-cap', 80, '2018-12-02 16:25:12'),
                (392, 'BTC', 4165.61, 'binance', 81, '2018-12-02 16:26:13'),
                (393, 'ETH', 117.06, 'binance', 81, '2018-12-02 16:26:13'),
                (394, 'LTC', 33.6997849, 'binance', 81, '2018-12-02 16:26:13'),
                (395, 'BTC', 4151.50091003, 'coin-market-cap', 81, '2018-12-02 16:26:13'),
                (396, 'ETH', 116.560776528, 'coin-market-cap', 81, '2018-12-02 16:26:13'),
                (397, 'LTC', 33.6024432942, 'coin-market-cap', 81, '2018-12-02 16:26:13'),
                (398, 'BTC', 4166.39, 'binance', 82, '2018-12-02 16:27:14'),
                (399, 'ETH', 117.01, 'binance', 82, '2018-12-02 16:27:14'),
                (400, 'LTC', 33.7060951, 'binance', 82, '2018-12-02 16:27:14'),
                (401, 'BTC', 4144.3773, 'coin-market-cap', 82, '2018-12-02 16:27:14'),
                (402, 'ETH', 116.560776528, 'coin-market-cap', 82, '2018-12-02 16:27:14'),
                (403, 'LTC', 33.6975785401, 'coin-market-cap', 82, '2018-12-02 16:27:14'),
                (404, 'BTC', 4165.32, 'binance', 83, '2018-12-02 16:28:15'),
                (405, 'ETH', 117, 'binance', 83, '2018-12-02 16:28:15'),
                (406, 'LTC', 33.72243072, 'binance', 83, '2018-12-02 16:28:15'),
                (407, 'BTC', 4144.3773, 'coin-market-cap', 83, '2018-12-02 16:28:15'),
                (408, 'ETH', 116.560776528, 'coin-market-cap', 83, '2018-12-02 16:28:15'),
                (409, 'LTC', 33.6975785401, 'coin-market-cap', 83, '2018-12-02 16:28:15'),
                (410, 'BTC', 4170.18, 'binance', 84, '2018-12-02 16:29:16'),
                (411, 'ETH', 117.05, 'binance', 84, '2018-12-02 16:29:16'),
                (412, 'LTC', 33.74509656, 'binance', 84, '2018-12-02 16:29:16'),
                (413, 'BTC', 4144.3773, 'coin-market-cap', 84, '2018-12-02 16:29:16'),
                (414, 'ETH', 116.74677569, 'coin-market-cap', 84, '2018-12-02 16:29:16'),
                (415, 'LTC', 33.6975785401, 'coin-market-cap', 84, '2018-12-02 16:29:16'),
                (416, 'BTC', 4168.99, 'binance', 85, '2018-12-02 16:30:18'),
                (417, 'ETH', 117, 'binance', 85, '2018-12-02 16:30:18'),
                (418, 'LTC', 33.71879112, 'binance', 85, '2018-12-02 16:30:18'),
                (419, 'BTC', 4152.76517818, 'coin-market-cap', 85, '2018-12-02 16:30:18'),
                (420, 'ETH', 116.74677569, 'coin-market-cap', 85, '2018-12-02 16:30:18'),
                (421, 'LTC', 33.6832116095, 'coin-market-cap', 85, '2018-12-02 16:30:18'),
                (422, 'BTC', 4167.21, 'binance', 86, '2018-12-02 16:31:19'),
                (423, 'ETH', 117.02, 'binance', 86, '2018-12-02 16:31:19'),
                (424, 'LTC', 33.74606658, 'binance', 86, '2018-12-02 16:31:19'),
                (425, 'BTC', 4152.76517818, 'coin-market-cap', 86, '2018-12-02 16:31:19'),
                (426, 'ETH', 116.74677569, 'coin-market-cap', 86, '2018-12-02 16:31:19'),
                (427, 'LTC', 33.6832116095, 'coin-market-cap', 86, '2018-12-02 16:31:19'),
                (428, 'BTC', 4179.76, 'binance', 87, '2018-12-02 16:32:20'),
                (429, 'ETH', 117.26, 'binance', 87, '2018-12-02 16:32:20'),
                (430, 'LTC', 33.8351572, 'binance', 87, '2018-12-02 16:32:20'),
                (431, 'BTC', 4152.76517818, 'coin-market-cap', 87, '2018-12-02 16:32:20'),
                (432, 'ETH', 116.852504011, 'coin-market-cap', 87, '2018-12-02 16:32:20'),
                (433, 'LTC', 33.6832116095, 'coin-market-cap', 87, '2018-12-02 16:32:20'),
                (434, 'BTC', 4191.45, 'binance', 88, '2018-12-02 16:33:21'),
                (435, 'ETH', 117.46, 'binance', 88, '2018-12-02 16:33:21'),
                (436, 'LTC', 33.90463905, 'binance', 88, '2018-12-02 16:33:21'),
                (437, 'BTC', 4155.08792764, 'coin-market-cap', 88, '2018-12-02 16:33:21'),
                (438, 'ETH', 116.852504011, 'coin-market-cap', 88, '2018-12-02 16:33:21'),
                (439, 'LTC', 33.7084158813, 'coin-market-cap', 88, '2018-12-02 16:33:21'),
                (440, 'BTC', 4190.13, 'binance', 89, '2018-12-02 16:34:22'),
                (441, 'ETH', 117.41, 'binance', 89, '2018-12-02 16:34:22'),
                (442, 'LTC', 33.940053, 'binance', 89, '2018-12-02 16:34:22'),
                (443, 'BTC', 4155.08792764, 'coin-market-cap', 89, '2018-12-02 16:34:22'),
                (444, 'ETH', 116.866977067, 'coin-market-cap', 89, '2018-12-02 16:34:22'),
                (445, 'LTC', 33.7084158813, 'coin-market-cap', 89, '2018-12-02 16:34:22'),
                (446, 'BTC', 4192.88, 'binance', 90, '2018-12-02 16:35:23'),
                (447, 'ETH', 117.48, 'binance', 90, '2018-12-02 16:35:23'),
                (448, 'LTC', 33.96652088, 'binance', 90, '2018-12-02 16:35:23'),
                (449, 'BTC', 4166.16601334, 'coin-market-cap', 90, '2018-12-02 16:35:23'),
                (450, 'ETH', 116.866977067, 'coin-market-cap', 90, '2018-12-02 16:35:23'),
                (451, 'LTC', 33.7084158813, 'coin-market-cap', 90, '2018-12-02 16:35:23');
        ");

    echo "seed run successfully\n";
}



