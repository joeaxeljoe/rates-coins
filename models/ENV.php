<?php

class ENV {
    public static $env_data = array();
    
    public static function init() {
        $raw_env = array_filter(explode("\n", trim(file_get_contents('.ENV'))));
        array_walk($raw_env, function ($v) {
            $splittedValue = explode('=', $v);
            if(trim($splittedValue[0])) {
                ENV::add(trim($splittedValue[0]), (isset($splittedValue[1]) ? trim($splittedValue[1]) : ''));
            }
        });
    }

    public static function add($k, $v){
        self::$env_data[$k] = $v ?: '';
    }

    public static function __callstatic($k, $arguments){
        return self::$env_data[$k];
    }

    public static function get($k){
        return self::$env_data[$k];
    }
}





