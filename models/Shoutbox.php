<?php

class Shoutbox
{
    public static function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public static function random_color() {
        return Shoutbox::random_color_part() . Shoutbox::random_color_part() . Shoutbox::random_color_part();
    }


    public static function getLasts($db)
    {

//        $shoutbox = new DB\SQL\Mapper($db,'shoutbox');
//        $shoutbox->load(array('userID=?','tarzan'));

        return $db->exec("
            SELECT *
            FROM shoutbox
            ORDER BY id DESC
            limit 12
        ");

    }

}