<?php

use coinmarketcap\api\CoinMarketCap;
require 'lib/Binance.php';


class Rate
{
    public static function getCurrenciesTicker() {
        /*
            $timestamp  = time() * 1000;
            $recvWindow = 9999999;
            $post_data = [
                'timestamp'     => $timestamp,
                'recvWindow'    => $recvWindow ,
                'signature'     => hash_hmac ('SHA256', "timestamp={$timestamp}&recvWindow={$recvWindow}",BINANCE_SECRET_KEY)
            ];
            $query_string = http_build_query($post_data);
        */

        $binanceApi       = new Binance(ENV::BINANCE_SECRET_KEY(), ENV::BINANCE_X_MBX_APIKEY());
        $prices           = $binanceApi->prices();

        return [
            'binance'  => [
                'BTC' => "{$prices['BTCUSDT']}",
                'ETH' => "{$prices['ETHUSDT']}",
                'LTC' => ($prices['BTCUSDT'] * $prices['LTCBTC'])
            ],
            'coin-market-cap' => [
                'BTC' => CoinMarketCap::getCurrencyTicker('bitcoin', "USD")[0]['price_usd'],
                'ETH' => CoinMarketCap::getCurrencyTicker('ethereum', "USD")[0]['price_usd'],
                'LTC' => CoinMarketCap::getCurrencyTicker('litecoin', "USD")[0]['price_usd']
            ]
        ];
    }

    public static function updateCurrencies($db, $prices) {
        $db->exec("INSERT INTO `group_rate` (status) VALUES (1)");
        $group_id = $db->lastInsertId();

        $insert_sql_values = [];
        foreach($prices as $api => $dataAPI) {
            foreach($dataAPI as $coin => $price) {
                $insert_sql_values[] = "'{$coin}', '{$price}', '{$api}', '{$group_id}'";
            }
        }

        $db->exec("
          INSERT INTO `rate` (`currency`, `price_USD`, `source_api`, `group_rate_id`)
          VALUES             (".implode('),(', $insert_sql_values).")
        ");
    }

    public static function getCurrently($db)
    {

//        $user = new DB\SQL\Mapper($db,'users');
//        $user->load(array('userID=?','tarzan'));

        return $db->exec("
            SELECT *
            FROM rate
            order by id DESC
            limit 6
        ");

    }

    public static function getCoinsHistorical($db) {

        return $db->exec("
            SELECT G.id, currency, AVG(price_USD), G.create_at create_at FROM group_rate G
                JOIN rate R
                ON G.id = R.group_rate_id
            group by G.id, R.currency
            order by G.id DESC
            limit 36
        ");


        /*SELECT currency, AVG(price_USD) FROM group_rate G
            JOIN rate R
            ON G.id = R.group_rate_id
        group by G.id, R.currency
        order by G.id DESC
        limit 3*/

    }
}