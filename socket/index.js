const io = require('socket.io')();
const log = console.log;
const mysql = require('mysql');


if(!process.argv[2]) {
    return log('Failed to load database connection data');
}

let connectionData = {};
let connectionRawData = process.argv[2].split('&');
for(let idx in connectionRawData) {
    connectionData[connectionRawData[idx].split('=')[0]] = connectionRawData[idx].split('=')[1];
}


var con = mysql.createConnection({
    host: connectionData["localhost"],
    user: connectionData["user"],
    password: connectionData["password"],
    database: connectionData["database"]
});

con.connect(function(err) {
    if (err) throw err;
});

io.listen(3333 , function(){
    log(`Socket listening on *: 3333`);
});


let counter = 0;
io.on('connection', function(socket){
    console.log('a user connected');

    io.emit('counter', ++counter);



    socket.on('shoutbox.send', function(msg){

        let sql = "INSERT INTO `shoutbox` (`msg`, `user`, `color`) VALUES (?, ?, ?);";
        con.query(sql, [msg.msg, msg.user, msg.color],function (err, result) {
            if (err) throw err;
            io.emit('shoutbox.received', msg);
        });
    });



    socket.on('disconnect', function(){
        io.emit('counter', --counter);

    });
});