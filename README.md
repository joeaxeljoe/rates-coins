# rates_coins

### .ENV
You need to install a .ENV file in the root of your project
(You can browse the .ENV.example file)

### Composer Installations
```sh
$ composer install
```
### Npm Installations
```sh
$ npm i
```

### DB
You only need to manually open a database with a name that matches the one you provided in the ENV file

### Cron tasks
production mode - use cron and run cron_tasks/update_currencies.php file evry minute:
```sh
* * * * * /usr/local/bin/php path/to/project/cron_tasks/update_currencies.php
```
dev mode - use php initializer.php and add "--dev-mode" flag:
```sh
$ cd path/to/project
$ php initializer.php --dev-mode
```
Leave the command line open.

### migrations
just add "--migration" flag when you run rhe initializer
```sh
$ php initializer.php --dev-mode --migration
```
and if you want to reset the database:
```sh
$ php initializer.php --dev-mode --migration --reset
```
and if you want to run seed:
```sh
$ php initializer.php --dev-mode --migration --reset --seed
```

### socket
The shoutbox feature works on a socket. To activate the socket, add the "--socket" flag to the initializer
```sh
$ php initializer.php --dev-mode --migration --reset --seed --socket
```

### sass
You can run SASS manually:
```sh
$ npm install -g sass
$ sass css/main.sass css/main.css
```
Or you can run a development tool:
```sh
$ gulp sass:watch
```

### .htaccess
You must enter your leader file. Edit the last line in the .htaccess
```sh
RewriteRule .* /path/to/project/index.php [L,QSA]
```
It is necessary - for the internal router of the project


### Quick Start:
```sh
$ composer install
$ npm i
$ php initializer.php --dev-mode --migration --reset --seed --gulp --socket
```

**have fun 🙂**