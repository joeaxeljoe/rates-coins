<?php
require 'models/ENV.php';
ENV::init();

define('SOCKET', array_search('--socket', $argv));
define('GULP', array_search('--gulp', $argv));
define('MIGRATION', array_search('--migration', $argv));
define('RESET_DB', MIGRATION && array_search('--reset', $argv) &&  preg_match("/^[yY]{1}$/", readline("You ran a *reset* command. Are you sure you want to delete all data from the database? (y/n)\n")) );
define('SEED', array_search('--seed', $argv));
define('IS_DEV_MODE', array_search('--dev-mode', $argv));


if(MIGRATION) {
    require('lib/migrations/index.php');
}

$node_command = '';
if(SOCKET) {
    $node_command .= 'node socket/index.js "' .
        http_build_query([
            "host" => ENV::DB_HOST(),
            "database" => ENV::DB_NAME(),
            "user" => ENV::DB_USER(),
            "password" => ENV::DB_PASS()
        ])
        .'"'
    ;
}

if(GULP) {
    $node_command .= (SOCKET ? ' | ' : '') . 'gulp "sass:watch"';
}

if($node_command) {
    echo "Please run the following command using another command line:\n" . $node_command . "\n";
    //exec($node_command);
}

if(IS_DEV_MODE) {
    require('cron_tasks/update_currencies.php');
}











