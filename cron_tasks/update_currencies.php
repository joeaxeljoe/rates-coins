<?php
require 'vendor/autoload.php';
require 'models/Rate.php';


if(!class_exists('ENV')) {
    require 'models/ENV.php';
    ENV::init();
}

if(!isset($db)) {
    $DB_HOST = ENV::DB_HOST();
    $DB_PORT = ENV::DB_PORT();
    $DB_NAME = ENV::DB_NAME();

    $db = new DB\SQL (
        "mysql:host={$DB_HOST};port={$DB_PORT};dbname={$DB_NAME}",
        ENV::DB_USER(),
        ENV::DB_PASS()
    );
}

if(defined( 'IS_DEV_MODE' ) && IS_DEV_MODE) { // Do it yourself
    while(true) {
        new updateRatsByUSD($db);
        sleep(60);
    }
}
else { // Do it with cron
    new updateRatsByUSD();
}



class updateRatsByUSD {

    private $db;

    private  $prices = [];

    function __construct($db) {
        $this->db = $db;
        $this->prices = Rate::getCurrenciesTicker();
        echo "API ticker run successfully - " . date("Y-m-d H:i:s") . "\n";
    }

    function __destruct() {
        Rate::updateCurrencies($this->db, $this->prices);
    }
}
