
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon-16x16.png">

    <title>Pricing example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">

    <script src="//cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script>
        var socket = io('<?= ($SOCKET_URL) ?>:<?= ($SOCKET_PORT) ?>');
    </script>

    <!--<script src="/socket.io/socket.io.js"></script>-->
</head>

<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">Company name</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark counter-online" href="#">There are currently <b id="counter"></b> users online</a>
        <a class="p-2 text-dark" href="<?= ($BASE_URL) ?>/history">Historical rates</a>
        <a class="p-2 text-dark" href="<?= ($BASE_URL) ?>">Home</a>
    </nav>
    <a class="btn btn-outline-primary" href="#">Sign up</a>
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4 test-class" >Currency Rates</h1>
    <p class="lead">View Top Currency Rates * Data Analysis * User Community * Love Crypto? You've come to the right place * Talk about it</p>
</div>

<div class="container">
    <div class="card-deck mb-3 text-center">
        <?php foreach (($coinsData?:[]) as $coin): ?>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal"><?= ($coin['currency']) ?></h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">$<?= (round($coin['AVG'],4)) ?> <small class="text-muted">(AVG)</small></h1>
                    <ul class="list-unstyled mt-3 mb-4 text-left">
                        <li>Coinmarketcap: $<?= ($coin['coin-market-cap']) ?></li>
                        <li>Binance:       $<?= ($coin['binance']) ?></li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-outline-primary">Start trading in <?= ($coin['currency']) ?></button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>


    <div class="row">
        <div class="col-2">
            Want to say something? What do you think about the state of cryptographic currencies in the world? Have something to comment on?
        </div>
        <div class="col-4">
            <form id="shoutbox">
                <div class="form-group">
                    <label for="exampleInputPassword1">Nike</label>
                    <input class="form-control" id="exampleInputPassword1" placeholder="Nike name">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Your message</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your data with anyone else.</small>
                </div>
                <button type="submit" class="btn btn-primary">Submit shoutbox</button>
            </form>
        </div>
        <div class="col-6">
            Want to say something? What do you think about the state of cryptographic currencies in the world? Have something to comment on?
        </div>
    </div>



    <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
            <div class="col-12 col-md">
                <img class="mb-2" src="../../assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
                <small class="d-block mb-3 text-muted">&copy; JoeAxelrod 2019</small>
            </div>
            <div class="col-6 col-md">
                <h5>Features</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Cool stuff</a></li>
                    <li><a class="text-muted" href="#">Random feature</a></li>
                    <li><a class="text-muted" href="#">Team feature</a></li>
                    <li><a class="text-muted" href="#">Stuff for developers</a></li>
                    <li><a class="text-muted" href="#">Another one</a></li>
                    <li><a class="text-muted" href="#">Last time</a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>Resources</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Resource</a></li>
                    <li><a class="text-muted" href="#">Resource name</a></li>
                    <li><a class="text-muted" href="#">Another resource</a></li>
                    <li><a class="text-muted" href="#">Final resource</a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>About</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Team</a></li>
                    <li><a class="text-muted" href="#">Locations</a></li>
                    <li><a class="text-muted" href="#">Privacy</a></li>
                    <li><a class="text-muted" href="#">Terms</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>

<script>


    socket.emit('chat message', 'aaaaaaaaaaaaaaa');


    socket.on('counter', function(msg) {

        document.getElementById("counter").innerHTML = msg;

    });

    socket.on('chat message', function(msg) {
        console.log('aaa: ' + msg);
    });



    document.getElementById("shoutbox").addEventListener("submit", function(event){
        event.preventDefault()

        socket.emit('chat message', 'aaaaaaaaaaaaaaa');
    });


</script>
<script src="js/main.js"></script>

</body>
</html>
